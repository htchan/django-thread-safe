to test the race condition
1. `python manage.py runserver 9090` to start the server
2. `cd test` in another terminal and `python race_condition.py` to test the server with multi threading