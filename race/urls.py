from django.urls import path

from . import views as race_views

urlpatterns = [
    path('init/', race_views.init, name='init'),
    path('add/', race_views.add, name='add'),
    path('delete/', race_views.delete, name='delete'),
    path('load/', race_views.load, name='load',)
]
