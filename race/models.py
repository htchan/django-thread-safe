from django.db import models

# Create your models here.

class Counting(models.Model):
    cid = models.CharField(max_length=10, default='race')
    number = models.IntegerField(default=0)
