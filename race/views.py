from django.shortcuts import render
from django.http import HttpResponse
from django.db import transaction
from django.db.models import F

from . import models as race_models

# Create your views here.

@transaction.atomic
def add(request):
    # num = race_models.Counting.objects.get(cid='racing')
    # num.number = F('number') + 1
    # # num.number  = num.number + 1
    # num.save()
    num = race_models.Counting.objects.select_for_update().get(cid='racing')
    if (num.number == 2):
        return HttpResponse('blocked')
    num.number += 1
    num.save()
    return HttpResponse('OK')

def init(request):
    num = race_models.Counting.objects.create(
        cid = 'racing',
    )
    num.save()
    return HttpResponse('init')

def delete(request):
    nums = race_models.Counting.objects.filter(cid='racing')
    for num in nums: 
        num.delete()
    return HttpResponse('delete')

def load(request):
    num = race_models.Counting.objects.get(cid='racing')
    return HttpResponse(str(num.number))