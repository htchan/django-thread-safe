import urllib.request, threading, time

COUNT = 99
URL = 'http://localhost:9090/race-condition/'

def make_req(url):
    while (True):
        try:
            req = urllib.request.urlopen(url)
            return req.read()
        except Exception as e:
            print(e)
            time.sleep(0.5)

if __name__ == '__main__':
    make_req(URL + 'delete/')
    make_req(URL + 'init/')
    threads = []
    for i in range(COUNT):
        ### multi threading ###
        threads.append(threading.Thread(target=make_req, args=(URL + 'add/',)))
        threads[-1].start()

        ### signle threading ###
        # make_req(URL + 'add/')

    for thread in threads:
        thread.join()
    
    print(make_req(URL + 'load/'))
    print(make_req(URL + 'delete/'))
